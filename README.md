Task: Build a server that sends messages to a telegram channel/group using a telegram bot token.

Requirement:  
Between each message, there should be a minimum 5 seconds pause (a message per 5 seconds).
Also, messages can have one of these priorities Low, Medium, and High. Higher priority messages should go first in FIFO(First In First Out) manner before lower priority messages.

Message {
text: "Some message",
priority: ["low", "medium" or "high"]
}

step-1: Build a REST server that sends messages to a telegram channel/group.

step-2: Add swagger doc

step-3: Ratelimit - one message per 5 seconds

step-4: Send by priority

step-5: Monolith into microservices (API Gateway service and message service) using gRPC

step-6: Rewrite using RabbitMQ, ActiveMQ or Kafka

step-7: Deploy to serve