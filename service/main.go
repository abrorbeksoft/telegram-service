package service

import (
	"context"
	"gitlab.com/abrorbeksoft/telegram-service/genproto/telegram_service"
	"gitlab.com/abrorbeksoft/telegram-service/storage"
	"gitlab.com/abrorbeksoft/telegram-service/telegram"
)

type telegramService struct {
	telebot *telegram.Bot
}

func NewTelegramService(token string) *telegramService {
	return &telegramService{
		telebot: telegram.NewBot(token),
	}
}


func (t *telegramService) Send(ctx context.Context, message *telegram_service.SendMessage) (*telegram_service.Response, error) {
	_, err:=t.telebot.SendText(&telegram.Group{
		ChatId: message.Receiver,
	}, message.Message.Text)
	if err!=nil {
		return nil, err
	}

	return &telegram_service.Response{
		Text: "Message was sent",
		Status: 200,
	}, nil
}

func (t *telegramService) GetChats(ctx context.Context, request *telegram_service.EmptyRequest) (*telegram_service.AllChats, error) {
	return storage.Receivers(), nil
}

