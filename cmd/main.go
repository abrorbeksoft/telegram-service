package main

import (
	"fmt"
	"gitlab.com/abrorbeksoft/telegram-service/config"
	"gitlab.com/abrorbeksoft/telegram-service/genproto/telegram_service"
	"gitlab.com/abrorbeksoft/telegram-service/service"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	conf:=config.Load()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s",conf.TelegramServicePort))
	if err != nil {
		log.Panic(err.Error())
	}
	telegramService := service.NewTelegramService(conf.BotToken)
	s := grpc.NewServer()
	telegram_service.RegisterTelegramServiceServer(s, telegramService)
	if err:=s.Serve(lis); err != nil {
		log.Panic(err.Error())
	}
}
