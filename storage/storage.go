package storage

import telegram "gitlab.com/abrorbeksoft/telegram-service/genproto/telegram_service"

var Receivers = func() *telegram.AllChats {
	var resp telegram.AllChats;
	resp.Chats = append(resp.Chats, &telegram.Chat{
			Id : "-1001709967327",
			Type : "supergroup",
			Title : "Test",
	})

	return &resp
}
