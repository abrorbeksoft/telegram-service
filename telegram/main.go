package telegram

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Bot struct {
	Token  string
	Client *http.Client
}

func NewBot(token string) *Bot {
	return &Bot{
		Token: token,
		Client: http.DefaultClient,
	}
}

func (bot *Bot) SendText(rec receiver, message string) ([]byte,error)  {

	requestBody, err:=json.Marshal(map[string]string{
		"chat_id": rec.getChatId(),
		"text": message,
	})
	if err!=nil{
		return nil,err
	}

	bytesBody:=bytes.NewBuffer(requestBody)
	res,err:=bot.Client.Post(fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage",bot.Token),"application/json", bytesBody)
	if err!=nil{
		return nil,err
	}

	defer res.Body.Close()
	data, err:= ioutil.ReadAll(res.Body)
	if err!=nil{
		return nil,err
	}

	return data, nil
}

type receiver interface {
	getChatId() string
}

type Channel struct {
	ChatId string
}

func (ch *Channel) getChatId() string {
	return ch.ChatId
}

type Group struct {
	ChatId string
}

func (ch *Group) getChatId() string {
	return ch.ChatId
}
