module gitlab.com/abrorbeksoft/telegram-service

go 1.16

require (
	github.com/joho/godotenv v1.4.0
	github.com/spf13/cast v1.4.1
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)
